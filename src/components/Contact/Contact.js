import React from 'react';
import PropTypes from 'prop-types';
import './Contact.css';
import { Helmet } from 'react-helmet';

const Contact = () => (
  <div className="Contact">
    <Helmet>
        <title>About</title>
    </Helmet>
    Contact Component
  </div>
);

Contact.propTypes = {};

Contact.defaultProps = {};

export default Contact;
