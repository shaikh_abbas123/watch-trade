import React from 'react';
import PropTypes from 'prop-types';
import './About.css';
import { Helmet } from 'react-helmet';


const About = () => 
(
  <div className="About">
    <Helmet>
        <title>About</title>
    </Helmet>
    About Component
  </div>
);

About.propTypes = {};

About.defaultProps = {};

export default About;
