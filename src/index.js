import React from 'react';  
import ReactDOM from 'react-dom';  
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'  
import './index.css';  
import App from './App';  
import About from './components/About/About'  
import Contact from './components/Contact/Contact'  

  
const routing = (  
  <Router>  
    
      <Route path="/" component={App} />  
      <Route path="/about" component={About} />  
      <Route path="/contact" component={Contact} />  
    
  </Router>  
)  
ReactDOM.render(routing, document.getElementById('root'));  